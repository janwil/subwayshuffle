var RED = 0;    //We will define our own color codes for convenience
var BLUE = 1;
var AQUA = 2;
var GREEN = 3;
var PURPLE = 4;
var YELLOW = 5;

var colorCodes = ["#ff0000","#0000ff","#00ffff","#00ff00","#ff00ff","#ffff00"];

var lineWidth = 5;

// Station functions

// We make one class for both the final aln ordinary stations.

function Station(stationNr,coordX,coordY) { //coordX and coordY refer to the coordinates of the centers
    this.stationNr = stationNr;
    this.isFinal = (stationNr === 0);             //isFinal shows whether the station is final or not
                                                // ... and it is iff it is listed as the first station
    this.isEmpty = true;                //isEmpty shows whether there is a train in the station or not
    if (this.isFinal) {
        this.srcX = 360;
        this.srcY = 610;
        this.width = 52;
        this.height = 52;
    } else {
        this.srcX = 300;
        this.srcY = 610;
        this.width = 48;
        this.height = 48;       
    }
    this.coordX = coordX;
    this.coordY = coordY;
    this.drawX = coordX-this.width/2;  //drawX and drawY are the cordinates for the upper left corner
    this.drawY = coordY-this.height/2;
    this.sequenceNumber = -1; // We will use this field for export functionality
}

Station.prototype.draw = function(ctx,imageSprite) {
    ctx.drawImage(imageSprite,this.srcX,this.srcY,this.width,this.height,this.drawX,this.drawY,this.width,this.height);
};

function drawStations(ctx,imageSprite) {
    for (var i = 0; i<stations.length; i++) {
        stations[i].draw(ctx,imageSprite);
    }
}

// End of Station functions


// Line functions

function Line(startStation,stopStation,colorNr){
    this.startStation = startStation;
    this.stopStation = stopStation;
    this.colorNr = colorNr;
    this.color  = colorCodes[colorNr];
}

Line.prototype.draw = function(ctx,lineWidth) {
    ctx.lineWidth = lineWidth;
    ctx.beginPath();
    ctx.moveTo(this.startStation.coordX,this.startStation.coordY);
    ctx.lineTo(this.stopStation.coordX,this.stopStation.coordY);
    ctx.strokeStyle = this.color;
    ctx.stroke();
};

function drawLines(ctx) {
    for (var i = 0; i<lines.length; i++) {
        lines[i].draw(ctx,lineWidth);
    }
}

// End of Line functions


// Train functions 

function Train(station,colorNr) { //Which station it begins from and what color it is
    this.station = station;
    this.stationNr = station.stationNr; // At which station the train currently is
    this.colorNr = colorNr;
    this.color = colorCodes[colorNr];
    this.srcX = colorNr*50;
    this.srcY = 610;
    this.width = 40;
    this.height = 40;
    this.coordX = this.station.coordX; //coordX and coordY specify the center point
    this.coordY = this.station.coordY;
    this.drawX = this.coordX-this.width/2;
    this.drawY = this.coordY-this.height/2;
}

Train.prototype.draw = function(ctx,imageSprite) {
    ctx.drawImage(imageSprite,this.srcX,this.srcY,this.width,this.height,this.drawX,this.drawY,this.width,this.height);
};

function drawTrains(ctx,imageSprite) {
    ctx.clearRect(0,0,gameWidth,gameHeight); 
    for (var i = 0; i<trains.length; i++) {
        trains[i].draw(ctx,imageSprite);
    }
}

function drawTrainsEditor(ctx,imageSprite) {
    for (var i = 0; i<trains.length; i++) {
        ctx.drawImage(imageSprite,trains[i].srcX,trains[i].srcY,trains[i].width,trains[i].height,trains[i].station.coordX-(trains[i].width)/2,trains[i].station.coordY-(trains[i].height)/2,trains[i].width,trains[i].height);
    }
}

// End of Train functions