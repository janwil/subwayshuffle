var canvasBg = document.getElementById('canvasBg');
var ctxBg = canvasBg.getContext('2d');

var canvasTrain = document.getElementById('canvasTrain');
var ctxTrain = canvasTrain.getContext('2d');


var gameWidth = canvasBg.width;
var gameHeight = canvasBg.height;

var imgSprite = new Image();
imgSprite.src = "images/sprite.png";
imgSprite.addEventListener('load',init,false);

var requestAnimFrame = 	window.requestAnimationFrame ||
	window.webkitRequestAnimationFrame ||
	window.mozRequestAnimationFrame ||
	window.msRequestAnimationFrame ||
	window.oRequestAnimationFrame ||
	function(callback) {
		window.setTimeout(callback, 1000/60);
	};

var lines = [];
var stations = [];
var trains = [];

var trainToMove;
var isDragging = false; // Shows whether we are currently dragging any trains

var problems = [
//[[[600,300],[500,350],[500,250],[400,400],[400,300],[400,200],[300,350],[300,250],[200,300]],[[8,7,0],[7,5,0],[5,4,0],[4,3,0],[3,1,0],[1,0,0],[7,4,3],[4,1,3],[6,4,1],[4,2,1],[5,2,2],[2,0,2],[3,6,2],[6,8,2]],[[8,0],[1,3],[7,3],[6,1],[2,1],[5,2],[3,2]]],                   //[ //Problem 
                   //     [[400,150,false],[506,194,false],[550,300,false],[506,406,false],
                   //      [400,450,true],[294,406,false],[250,300,false],[294,194,false]], //Stations
                   //     [[0,3,RED],[3,6,RED],[6,1,RED],[1,4,RED],[0,5,RED],[5,2,RED],[2,7,RED],[7,4,RED],
                   //      [1,2,BLUE],[3,4,BLUE],[5,6,BLUE],[7,0,BLUE],[0,4,BLUE],[1,5,BLUE],[3,7,BLUE],[2,6,BLUE],
                   //      [1,3,GREEN],[3,5,GREEN],[5,7,GREEN],[7,1,GREEN],
                   //      [0,2,PURPLE],[2,4,PURPLE],[4,6,PURPLE],[6,0,PURPLE]], //Lines
                   //     [[0,RED],[1,GREEN],[2,PURPLE],[3,BLUE],[6,PURPLE],[5,GREEN],[7,BLUE]] //Trains
                   //],
                  [ //Problem 
                        [[500,300],[400,300],[300,300],[400,200]], //Stations
                            //Station number 0 is always the final one
                            //The stations are defined by their coordinates on the canvas
                        [[0,1,RED],[1,2,RED],[1,3,BLUE]], //Lines
                            //Stations number 0 and 1 are joined by a red line, etc.
                        [[2,RED],[1,BLUE]] //Trains
                            //In station number 2 there is a red train, etc.
                   ],
                   [ //Problem 
                        [[550,250],[450,250],[350,300],[350,200],[250,250]], //Stations
                        [[0,1,RED],[1,2,BLUE],[1,3,RED],[2,3,RED],[2,4,RED],[3,4,GREEN]], //Lines
                        [[4,RED],[2,BLUE],[3,GREEN]] //Trains
                   ],
                       [[[500,200],[450,300],[350,300],[400,200],[300,200],[400,400]],[[0,1,0],[1,2,0],[0,3,1],[3,4,1],[4,1,1],[3,1,3],[2,4,3],[2,5,0],[1,5,1]],[[0,1],[1,0],[3,3],[2,3]]],
                   [ //Problem 
                        [[550,300],[400,200],[450,300],
                         [350,300],[400,400],[250,300]], //Stations
                        [[0,1,RED],[1,4,RED],[4,3,RED],[3,5,RED],
                         [1,3,GREEN],[2,4,GREEN],[4,5,GREEN],[1,5,GREEN],
                         [1,2,BLUE]], //Lines
                        [[1,BLUE],[2,GREEN],[4,GREEN],[5,RED]] //Trains
                   ],
                   //[ //Problem 
                   //     [[400,180,false],[310,220,false],[290,320,false],[350,400,false],[450,400,true],
                   //      [510,320,false],[490,220,false]],//Stations
                   //     [[1,3,RED],[1,5,RED],[5,0,RED],[0,2,RED],[2,6,RED],[6,4,RED],
                   //      [4,0,YELLOW],[0,1,YELLOW],[5,2,BLUE],[0,3,BLUE],[2,3,BLUE],
                   //      [3,6,GREEN],[4,1,GREEN],[5,4,GREEN]],//Lines
                   //     [[1,GREEN],[6,GREEN],[2,BLUE],[5,BLUE],[3,RED],[4,YELLOW]]//Trains
                   //],                   
                   //[ //Problem 
                   //     [[550,300],[450,300],[350,300],[250,300],
                   //      [250,200],[350,200],[350,400]], //Stations
                   //     [[0,1,RED],[1,2,RED],[2,3,RED],[2,4,RED],
                   //      [2,6,BLUE],[1,6,GREEN],[4,5,GREEN],[5,2,BLUE],[0,5,BLUE]], //Lines
                   //     [[0,BLUE],[1,GREEN],[2,BLUE],[4,RED],[5,GREEN]] //Trains
                   //],
                  [ //Problem 
                        [[600,300],[500,400],[500,200],[400,300],[300,400],[300,200],[200,300]], //Stations
                        [[0,1,BLUE],[0,2,RED],[1,3,GREEN],[2,3,RED],[1,4,BLUE],
                         [2,5,BLUE],[3,4,RED],[3,5,GREEN],[4,6,RED],[5,6,BLUE]], //Lines
                        [[6,RED],[4,BLUE],[5,GREEN],[1,GREEN],[2,BLUE]] //Trains
                            //In station number 2 there is a red train, etc.
                   ],
                   [ //Problem 
                        [[600,300],[500,250],[500,350],[400,200],[400,300],
                          [400,400],[300,250],[300,350],[200,300]], //Stations
                        [[0,2,RED],[2,5,RED],[5,4,RED],[4,3,RED],[3,6,RED],[6,8,RED],
                         [1,4,GREEN],[4,7,GREEN],[2,4,BLUE],[4,6,BLUE],
                         [3,1,AQUA],[1,0,AQUA],[5,7,AQUA],[7,8,AQUA]], //Lines
                        [[8,RED],[6,BLUE],[2,BLUE],[1,GREEN],[7,GREEN],[3,AQUA],[5,AQUA]] //Trains
                   ]
               ];
var problemNr = 0;
var currentProblem;
var problemSolved = false;

document.addEventListener('mousedown',mouseDown,false);
document.addEventListener('mouseup',mouseUp,false);

// Main functions

function init() {
    drawBG();
    stations = [];
    lines = [];
    trains = [];
    currentProblem = problems[problemNr];
    initiateStations();
    initiateLines();
    drawLines(ctxBg);
    drawStations(ctxBg,imgSprite);
    initiateTrains();
    loop();
}

function loop(){
    drawTrains(ctxTrain,imgSprite);
    if (problemSolved) {
        alert("Tubli! Tahad uut?");
        problemSolved = false;
        problemNr = (problemNr + 1) % problems.length;
        init();
    }
    requestAnimFrame(loop);
}

function nextProblem(){
    problemNr = (problemNr + 1) % problems.length;
    init();
}

function previousProblem(){
    problemNr = (problems.length + problemNr - 1) % problems.length;
    init();
}

function drawBG () {
    ctxBg.clearRect(0,0,gameWidth,gameHeight);
    ctxBg.drawImage(imgSprite,0,0,gameWidth,gameHeight,0,0,gameWidth,gameHeight);
}


function initiateStations() {
    var problemStations = currentProblem[0];
    for(var i = 0; i < problemStations.length; i++) {
        var currentStation = problemStations[i];
        stations[i] = new Station(i,currentStation[0],currentStation[1]);
    }
}

function initiateLines() {
    var problemLines = currentProblem[1];
    for(var i = 0; i < problemLines.length; i++) {
        var currentLine = problemLines[i];
        lines[i] = new Line(stations[currentLine[0]],stations[currentLine[1]],currentLine[2]);
    }
}

//function drawLines() {
//    for (var i = 0; i<lines.length; i++) {
//        lines[i].draw(ctxBg,lineWidth);
//    }
//}

//function drawStations() {
//    for (var i = 0; i<stations.length; i++) {
//        stations[i].draw(ctxBg,imgSprite);
//    }
//}

function initiateTrains() {
    var problemTrains = currentProblem[2];
    for(var i = 0; i < problemTrains.length; i++) {
        var currentTrain = problemTrains[i];
        trains[i] = new Train(stations[currentTrain[0]],currentTrain[1]);
        stations[currentTrain[0]].isEmpty = false;
    }
}

//function drawTrains() {
//    ctxTrain.clearRect(0,0,gameWidth,gameHeight);
//    for (var i = 0; i<trains.length; i++) {
//        trains[i].draw(ctxTrain,imgSprite);
//    }
//}

// End of main functions

// Helping functions

function stationsAreConnected(nr1,nr2,color) { //This function checks whether there is a line of the given color
                                               // between the stations with given numbers
    var areConnected = false;
    for(var i = 0; i < lines.length; i++) {
        if (((lines[i].startStation.stationNr === nr1 && lines[i].stopStation.stationNr === nr2) ||
            (lines[i].startStation.stationNr === nr2 && lines[i].stopStation.stationNr === nr1) ) &&
            lines[i].color === color) {
            areConnected = true;
        }
    }
    return areConnected;
}

// End of helping functions

// Event functions

function mouseDown(e) {
    mouseX = e.pageX - canvasBg.offsetLeft;
	mouseY = e.pageY - canvasBg.offsetTop;
    for(var i = 0; i < trains.length; i++){
        if (mouseX >= trains[i].drawX && mouseX <= trains[i].drawX+trains[i].width &&
            mouseY >= trains[i].drawY && mouseY <= trains[i].drawY+trains[i].height) {
            trainToMove = trains[i];
            isDragging = true;
            document.onmousemove = myMove;
        }
    }
}

function mouseUp(e) {
    document.onmousemove = null;
    if (isDragging) {
        mouseX = e.pageX - canvasBg.offsetLeft;
        mouseY = e.pageY - canvasBg.offsetTop; 
        for(var i = 0; i < stations.length; i++){ 
            if (    mouseX >= stations[i].drawX &&      //We check whether the mouse was released over a vacant station
                    mouseX <= stations[i].drawX+stations[i].width &&
                    mouseY >= stations[i].drawY &&
                    mouseY <= stations[i].drawY+stations[i].height &&
                    stations[i].isEmpty &&
                    stationsAreConnected(trainToMove.stationNr,i,trainToMove.color)) { // ... and that the stations are connected by
                                                                                        // a line of correct color
                stations[trainToMove.stationNr].isEmpty = true; //We move the train away from the original station
                stations[i].isEmpty = false; //We move the train to the new station
                trainToMove.stationNr = i;
                trainToMove.drawX = stations[i].coordX - (trainToMove.width)/2;
                trainToMove.drawY = stations[i].coordY - (trainToMove.height)/2;
                if ((trainToMove.color === colorCodes[RED]) && (stations[i].isFinal)) {
                    problemSolved = true;
                }
            } else {
                trainToMove.drawX = stations[trainToMove.stationNr].coordX - (trainToMove.width)/2;
                trainToMove.drawY = stations[trainToMove.stationNr].coordY - (trainToMove.height)/2;
            }
        }
        isDragging = false;
    }
}

function myMove(e) {
    trainToMove.drawX = e.pageX - canvasBg.offsetLeft - (trainToMove.width)/2;
    trainToMove.drawY = e.pageY - canvasBg.offsetTop - (trainToMove.height)/2;
}

// End of event functions