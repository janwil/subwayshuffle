var canvasBg = document.getElementById('canvasBg');
var ctxBg = canvasBg.getContext('2d');

var canvasLevel = document.getElementById('canvasLevel');
var ctxLevel = canvasLevel.getContext('2d');

var gameWidth = canvasBg.width;
var gameHeight = canvasBg.height;

var imgSprite = new Image();
imgSprite.src = "images/sprite.png";
imgSprite.addEventListener('load',init,false);

var rightFinalStation = new Station(0,gameWidth-50,50);
var rightStation = new Station(1,gameWidth-50,125);

var stationToMove;

var requestAnimFrame = 	window.requestAnimationFrame ||
	window.webkitRequestAnimationFrame ||
	window.mozRequestAnimationFrame ||
	window.msRequestAnimationFrame ||
	window.oRequestAnimationFrame ||
	function(callback) {
		window.setTimeout(callback, 1000/60);
	};

var lines = [];
var stations = [];
var trains = [];

var numberOfStations = 42; //The number of stations to generate and use
var visibleRegularStations = 0; //We will keep count of the regular stations visible on the drawing area

document.addEventListener('mousedown',mouseDown,false);
document.addEventListener('mouseup',mouseUp,false);
document.addEventListener('click',mouseClicked,false);

var MODE_NOTHING = 0;
var MODE_DRAGGING_STATION = 1;
var draggingFinalStation = false; //Used to distinguish whether we drag a final or a regular station

var MODE_DRAWING_LINE = 2;
var drawingLineColor;
var firstStationSelected = false;   //Used to distinguish between the two mouse clicks needed to define a line
var firstStationNumber;     //Once the first station is clicked, its number is stored here

var MODE_DRAWING_TRAIN = 3;
var drawingTrainColor;

var currentMode = MODE_NOTHING;

var problemData;

var positionsAnalyzed;
var maxDepth = 25;                          // The number of moves we will attempt to make
var solutionDepth;

// Main functions

function init() {
    drawBG();
    initiateStations();
    loop();
}

function drawBG () {
    ctxBg.clearRect(0,0,gameWidth,gameHeight);
    ctxBg.drawImage(imgSprite,0,0,gameWidth,gameHeight,0,0,gameWidth,gameHeight);
    for(var i=50; i <= gameWidth-100; i+=50 ){          //Draw the grid
        ctxBg.lineWidth = 1;
        ctxBg.beginPath();
        ctxBg.moveTo(i,0);
        ctxBg.lineTo(i,gameHeight);
        ctxBg.strokeStyle = "#505050";
        ctxBg.stroke();
    }
    for(var j=50; j < gameHeight; j+=50){
        ctxBg.lineWidth = 1;
        ctxBg.beginPath();
        ctxBg.moveTo(0,j);
        ctxBg.lineTo(gameWidth-100,j);
        ctxBg.strokeStyle = "#505050";
        ctxBg.stroke();        
    }
    rightFinalStation.draw(ctxBg,imgSprite);            //Draw the toolbox stations
    rightStation.draw(ctxBg,imgSprite);
    for(var i = 0; i < 6; i++){                         //Draw the color toolbox lines
        ctxBg.lineWidth = 10;
        ctxBg.beginPath();
        ctxBg.moveTo(gameWidth-75,175+i*25);
        ctxBg.lineTo(gameWidth-25,175+i*25);
        ctxBg.strokeStyle = colorCodes[i];
        ctxBg.stroke();
    }
    for(var i = 0; i < 6; i++){                         //Draw the toolbox trains
        var srcX = i*50;
        var srcY = 610;
        var width = 40;
        var height = 40;
        var coordX = gameWidth - 50;
        var coordY = 345 + i*45;
        var drawX = coordX-width/2;
        var drawY = coordY-height/2;
        ctxBg.drawImage(imgSprite,srcX,srcY,width,height,drawX,drawY,width,height);
    }
}

function initiateStations() {
    for(var i = 0; i < numberOfStations; i++) {
        stations[i] = new Station(i,-100,100);
    }
}

function loop(){
    ctxLevel.clearRect(0,0,gameWidth,gameHeight);
    drawEditorFrame();
    drawLines(ctxLevel);
    drawStations(ctxLevel,imgSprite);
    drawTrainsEditor(ctxLevel,imgSprite);
    requestAnimFrame(loop);
}

function drawEditorFrame() {
    switch (currentMode) {
        case (MODE_DRAGGING_STATION) : {
            if (draggingFinalStation) {
                drawEditBox(722,22,778,78); //Should rewrite these relative to gameWidth
            } else {
                drawEditBox(724,101,776,149);
            }
            break;
        }
        case (MODE_DRAWING_LINE) : {
            drawEditBox(gameWidth-77,168+drawingLineColor*25,gameWidth-23,182+drawingLineColor*25);
            break;
        }
        case (MODE_DRAWING_TRAIN) : {
            drawEditBox(gameWidth-72,323 + drawingTrainColor*45,gameWidth-28,367 + drawingTrainColor*45);
            break;
        }
    }
}

function drawEditBox(x1,y1,x2,y2) {
    ctxLevel.lineWidth = 4;
    ctxLevel.strokeStyle = "#808080";
    ctxLevel.beginPath();
    ctxLevel.moveTo(x1,y1);
    ctxLevel.lineTo(x2,y1);
    ctxLevel.lineTo(x2,y2);
    ctxLevel.lineTo(x1,y2);
    ctxLevel.lineTo(x1,y1);
    ctxLevel.stroke();
}

// Event functions

function mouseDown(e) {
    mouseX = e.pageX - canvasBg.offsetLeft;
	mouseY = e.pageY - canvasBg.offsetTop;
    switch (currentMode) {
        case MODE_NOTHING : {
            if (mouseX >= 0 && mouseX <= gameWidth - 100 && mouseY >= 0 && mouseY <= gameHeight) {
                        //We clicked in the drawing area
                for (var i = 0; i < stations.length; i++) {
                    if (mouseX >= stations[i].drawX && mouseX <= stations[i].drawX+stations[i].width &&
                        mouseY >= stations[i].drawY && mouseY <= stations[i].drawY+stations[i].height) {
                            //We selected a station from the drawing area
                        stationToMove = stations[i];
                        currentMode = MODE_DRAGGING_STATION;
                        draggingFinalStation = (i === 0);
                        document.onmousemove = myMove;
                    }
                }
            } else {    //We clicked in the toolbox area or outside of the canvas
                if (mouseX >= rightFinalStation.drawX && mouseX <= rightFinalStation.drawX+rightFinalStation.width &&
                    mouseY >= rightFinalStation.drawY && mouseY <= rightFinalStation.drawY+rightFinalStation.height) {
                            //We selected the final station from the tool box
                    stationToMove = stations[0];
                    currentMode = MODE_DRAGGING_STATION;
                    draggingFinalStation = true;
                    document.onmousemove = myMove;
                }
                if (mouseX >= rightStation.drawX && mouseX <= rightStation.drawX+rightStation.width &&
                    mouseY >= rightStation.drawY && mouseY <= rightStation.drawY+rightStation.height) {
                            //We selected a regular station from the tool box
                    if (visibleRegularStations < numberOfStations-1) { //If we have max number of stations already, do nothing
                        var i = 1;
                        while (!(stations[i].coordX === -100 || stations[i].drawX === -100)) {
                            i++;  //The stations with either coordX === -100 or drawX === -100 are the ones not visible
                        }           //So we loop to find the first currently non-visible regular station
                        visibleRegularStations++;
                        stationToMove = stations[i];
                        currentMode = MODE_DRAGGING_STATION;
                        draggingFinalStation = false;
                        document.onmousemove = myMove;
                    }
                }
            }
            break;
        }
    }
}

function mouseUp(e) {
    switch (currentMode) {
        case  MODE_DRAGGING_STATION : {
            document.onmousemove = null;
            mouseX = e.pageX - canvasLevel.offsetLeft;
            mouseY = e.pageY - canvasLevel.offsetTop;
            if (mouseX >= gameWidth-100 || mouseX <= 0 || //The station was dragged outside of the drawing area
                mouseY >= gameHeight || mouseY <= 0) {      //So we delete the station from the canvas by moving it out
                var i = 0;                                  //We also delete the lines connected to this station
                while (lines.length > i) {
                    if (lines[i].startStation.stationNr === stationToMove.stationNr || lines[i].stopStation.stationNr === stationToMove.stationNr) {
                        lines.splice(i,1);
                    } else {
                        i++;
                    }
                }
                var j = 0;
                while (trains.length > j) {             //We also search whether there was a train in the deleted station
                    if (trains[j].station.stationNr === stationToMove.stationNr) {
                        trains.splice(j,1);          // ... and if there was, delete it
                        break;                      //There can be no more than one train in this station
                    } else {
                        j++;
                    }
                }
                stationToMove.drawX = -100;                
                stationToMove.drawY = 100;
                if (!draggingFinalStation) {
                    visibleRegularStations--;
                }
            } else {                                       //We are inside and grip the station to the grid
                var rawX = e.pageX - canvasLevel.offsetLeft;
                var rawY = e.pageY - canvasLevel.offsetTop;
                rawX = 50*(Math.round(rawX/50));
                rawY = 50*(Math.round(rawY/50));
                if (rawX === 0) rawX = 50;
                if (rawX === gameWidth - 100) rawX = gameWidth - 150;
                if (rawY === 0) rawY = 50;
                if (rawY === gameHeight) rawY = gameHeight - 50;
                stationToMove.coordX = rawX;
                stationToMove.coordY = rawY;
                stationToMove.drawX = rawX - (stationToMove.width)/2;
                stationToMove.drawY = rawY - (stationToMove.height)/2;
            }
            currentMode = MODE_NOTHING;
            break;
        }
    }
}

function myMove(e) {
    switch (currentMode) {
        case MODE_DRAGGING_STATION : {
            stationToMove.coordX = e.pageX - canvasLevel.offsetLeft;
            stationToMove.coordY = e.pageY - canvasLevel.offsetTop;
            stationToMove.drawX = stationToMove.coordX - (stationToMove.width)/2;
            stationToMove.drawY = stationToMove.coordY - (stationToMove.height)/2;
            break;
        }
    }
}

function mouseClicked(e) {
    mouseX = e.pageX - canvasBg.offsetLeft;
    mouseY = e.pageY - canvasBg.offsetTop;
    switch (currentMode) {
        case MODE_NOTHING : {
            if (mouseX >= gameWidth-75 && mouseX <= gameWidth-25 && mouseY >= 170 && mouseY < 305) {
                                        //We are within the color line toolbox
                var tmpY = mouseY - 170;
                var remainderY = tmpY % 25;
                if (remainderY < 10) { //We clicked a color bar
                    currentMode = MODE_DRAWING_LINE;
                    drawingLineColor = Math.floor(tmpY/25); //We use the fact that the bars are in order
                    firstStationSelected = false;
                }
            }
            if (mouseX >= gameWidth-70 && mouseX <= gameWidth-30 && mouseY >= 325 && mouseY < 590) {
                                        //We are within the color train toolbox
                var tmpY = mouseY - 325;
                var remainderY = tmpY % 45;
                if (remainderY < 40) { //We clicked a color train
                    currentMode = MODE_DRAWING_TRAIN;
                    drawingTrainColor = Math.floor(tmpY/45); //We use the fact that the trains are in order
                }
            }            
            break;
        }
        case MODE_DRAWING_LINE : {
            var stationWasClicked = false;
            var numberOfStationClicked;
            for(var i=0; i < numberOfStations; i++) {
                if (mouseX >= stations[i].drawX && mouseX <= stations[i].drawX + stations[i].width &&
                    mouseY >= stations[i].drawY && mouseY <= stations[i].drawY + stations[i].height &&
                    mouseX >= 0 && mouseX <= gameWidth - 100 && mouseY >= 0 && mouseY <= gameHeight) {
                                    //We clicked on a station within the drawing area
                    stationWasClicked = true;
                    numberOfStationClicked = i;
                }
            }
            if (stationWasClicked) {                //We clicked on a visible station
                if (!firstStationSelected) {        //... and it is the first one
                    firstStationNumber = numberOfStationClicked;
                    firstStationSelected = true;
                } else {                            //... and it's the second one
                    lines[lines.length] = new Line (stations[firstStationNumber],stations[numberOfStationClicked],drawingLineColor);
                    firstStationSelected = false;
                    //currentMode = MODE_NOTHING;   //When this is commented out, we continue drawing lines
                }
            } else {                                //We just clicked on canvas
                currentMode = MODE_NOTHING;         //... so let's cancel drawing a line
            }
            break;
        }
        case MODE_DRAWING_TRAIN : {
            for(var i=0; i < numberOfStations; i++) {
                if (mouseX >= stations[i].drawX && mouseX <= stations[i].drawX + stations[i].width &&
                    mouseY >= stations[i].drawY && mouseY <= stations[i].drawY + stations[i].height &&
                    mouseX >= 0 && mouseX <= gameWidth - 100 && mouseY >= 0 && mouseY <= gameHeight) {
                                    //We clicked on a station within the drawing area
                                    //So we create a new train, but first we check if there is an old train in this station
                    var j = 0;
                    while (trains.length > j) {
                        if (trains[j].station.stationNr === i) {
                            trains.splice(j,1);          // ... and if there is, delete it
                            break;                      //There can be no more than one train in this station
                        } else {
                            j++;
                        }
                    }
                    trains[trains.length] = new Train(stations[i],drawingTrainColor);
                }
                currentMode = MODE_NOTHING;
            }
            break;
        }
    }
}

// End of event functions

// Export and analyze functions

function exportLevel() {
    var problemData = collectProblemData();
    printProblemData(problemData);
}

function analyzeLevel() {
    problemData = collectProblemData();
    analyzeProblemData(problemData);
}


// This function will turn the editor data structure into the problem data structure

function collectProblemData() {
    var currentSequenceNumber = 0;
    for(var i=0; i < numberOfStations; i++) {
        if (!(stations[i].drawX===-100) && !(stations[i].coordX===-100)) { //We have a real displayed station
            stations[i].sequenceNumber = currentSequenceNumber;     //We need to set continuous numbers to the stations
            currentSequenceNumber++;                                //since there may be gaps due to deletions
        } else {
            stations[i].sequenceNumber = -1;                    //This marks a non-displayed station
        }
    }
    var stationsList = [];
    var linesList = [];
    var trainsList = [];
    for(var i=0; i < numberOfStations; i++) {
        if (stations[i].sequenceNumber >= 0) {       //We have a real displayed station
            stationsList[stationsList.length] = [stations[i].coordX,stations[i].coordY];
        }
    }
    for(var i = 0; i < lines.length; i++) {
        linesList[linesList.length] = [lines[i].startStation.sequenceNumber,lines[i].stopStation.sequenceNumber,lines[i].colorNr];
    }
     for(var i = 0; i < trains.length; i++) {
        trainsList[trainsList.length] = [trains[i].station.sequenceNumber,trains[i].colorNr];
    }
   return [stationsList,linesList,trainsList];
}

//This function will print the problem data structure on screen to be copied 

function printProblemData(problem) {
    var stationsStr = "[";
    var linesStr = "[";
    var trainsStr = "[";
    
    for (var i = 0; i < problem[0].length; i++) {
        stationsStr += "[" + problem[0][i].toString() + "]";
        if (i < problem[0].length -1 ) {
            stationsStr += ",";
        }
    }
    stationsStr += "]";
    
        for (var i = 0; i < problem[1].length; i++) {
        linesStr += "[" + problem[1][i].toString() + "]";
        if (i < problem[1].length -1 ) {
            linesStr += ",";
        }
    }
    linesStr += "]";
    
        for (var i = 0; i < problem[2].length; i++) {
        trainsStr += "[" + problem[2][i].toString() + "]";
        if (i < problem[2].length -1 ) {
            trainsStr += ",";
        }
    }
    trainsStr += "]";
    
    var problemStr = "[" + stationsStr + "," + linesStr + "," + trainsStr + "]";

    document.getElementById("levelSource").innerHTML = problemStr;
}

//This is an object describing the problem state

function ProblemState(problem) {
    this.nrOfStations = problem[0].length;
    this.lines = problem[1];
    tmpTrains = [];
    for (var i = 0; i < this.nrOfStations; i++) {
        tmpTrains[tmpTrains.length] = -1;           //Default value for an empty station
    }
    for (var i = 0; i < problem[2].length; i++) {
        tmpTrains[problem[2][i][0]] = problem[2][i][1];  //We will have a list of kind [2,-1,-1,0,1,1,-1]
    }
    this.trains = tmpTrains;
}

//This function will take problem data structure and try to make all possible moves up to some depth

function analyzeProblemData (problem) {
    var initialState = new ProblemState(problem);
    var prohibitedMove = [-1,0];                //We do not want to undo the last move we tried, so we
                                                //need this argument in the recursive calls.
                                                //Initially, there is no prohibited move, so we just declare
                                                //an impossible move.
    positionsAnalyzed = 0;
    solutionDepth = maxDepth + 1;
    var success = tryToSolve(initialState,maxDepth,prohibitedMove);
    if (success) {
        alert('The problem is solvable in '+solutionDepth+' moves. Reached '+positionsAnalyzed+' positions.');
    } else {
        alert('The problem is NOT solvable within '+maxDepth+' moves. Reached '+positionsAnalyzed+' positions.');
    }
}

//This is a recursive function trying to dig into the solution tree

function tryToSolve(currentState,currentDepth,illegalMove) {
    positionsAnalyzed++;
    if (currentDepth <= maxDepth - solutionDepth) return false;               //We reached the maximal depth
    if (currentState.trains[0]===RED)  {                //Red train reached the final station
        if (solutionDepth > maxDepth - currentDepth) {
            solutionDepth = maxDepth - currentDepth;
        }
        return true;
    }                
    var stateList = makeAllMoves(currentState,illegalMove);
    var tmpResult = false;
    for (var i = 0; i < stateList.length; i++) {
        var solutionResult = tryToSolve(stateList[i][0],currentDepth-1,stateList[i][1]);
        tmpResult = tmpResult || solutionResult;
    }
    return tmpResult;
}

//This function will return a list of states occurring from the current one after all possible moves except for the
//given move. Next to each state, a move is returned showing which move led to this state (so that we can forbid)
//this in the next iteration.


function makeAllMoves(aState,prohibitedMove) {
    var tmpStateList = [];
    var currentLine;
    var vacantEndpoint;
    var occupiedEndpoint;
    var trainColor;
    for (var i = 0; i < aState.lines.length; i++) {
        currentLine = aState.lines[i];
        if ( ((aState.trains[currentLine[0]] === -1) &&  !(aState.trains[currentLine[1]] === -1)) ||
             ((aState.trains[currentLine[1]] === -1) &&  !(aState.trains[currentLine[0]] === -1)) ) {
                                    //Exactly one endpoint of the current line is vacant
            if (aState.trains[currentLine[0]] === -1) {
                vacantEndpoint = currentLine[0];
                occupiedEndpoint = currentLine[1];
                trainColor = aState.trains[currentLine[1]];
            } else {
                vacantEndpoint = currentLine[1];
                occupiedEndpoint = currentLine[0];
                trainColor = aState.trains[currentLine[0]];          
            }
            if ( (trainColor === currentLine[2]) &&
                 !( (prohibitedMove[0]===vacantEndpoint && prohibitedMove[1]===occupiedEndpoint) ||
                    (prohibitedMove[1]===vacantEndpoint && prohibitedMove[0]===occupiedEndpoint)) ){
                                //The stations are connected by the correct line color and the
                                //corresponding move is not prohibited
                var tmpState = new ProblemState([[],[],[]]);        //We have a new position
                tmpState.nrOfStations = aState.nrOfStations;
                tmpState.lines = aState.lines;
                for(var j = 0; j < aState.trains.length; j++) {
                    tmpState.trains[j] = aState.trains[j]; 
                }
                tmpState.trains[vacantEndpoint] = tmpState.trains[occupiedEndpoint]; //Move the train
                tmpState.trains[occupiedEndpoint] = -1;
                tmpStateList[tmpStateList.length] = [tmpState,[vacantEndpoint,occupiedEndpoint]];
            }
        }
    }
    return tmpStateList;
}

// End of export and analyze functions